
# DockerFlask
# https://gitlab.com/lowswaplab/dockerflask
# Copyright 2018 Low SWaP Lab lowswaplab.com

NAME = DockerFlask

all:	docker

run:	docker
		docker run --rm ${NAME}

sh:		docker
		docker run -it --rm ${NAME} /bin/sh

docker:	.FORCE
		docker build -t ${NAME} .

clean:  .FORCE
		-docker rmi ${NAME}

.FORCE:

