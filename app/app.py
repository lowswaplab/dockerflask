
# DockerFlask
# https://gitlab.com/lowswaplab/dockerflask
# Copyright 2018 Low SWaP Lab lowswaplab.com

from flask import Flask

application = Flask(__name__)

@application.route("/", defaults={"path": ""})
@application.route("/<path:path>")
def index(path):
    return("<span style='color:red'>Path: {}</span>".format(path))

if __name__ == "__main__":
  application.run(host="0.0.0.0", port=3030)

