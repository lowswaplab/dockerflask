
# DockerFlask
# https://gitlab.com/lowswaplab/dockerflask
# Copyright 2018 Low SWaP Lab lowswaplab.com

FROM alpine

COPY ./app /app
WORKDIR /app

#RUN apk add --no-cache py3-pip build-base linux-headers python3-dev pcre-dev
RUN apk add --no-cache py3-pip uwsgi-python3
RUN pip3 install --upgrade pip
RUN pip3 install flask
#RUN pip3 install flask uwsgi
#RUN apk del py3-pip build-base linux-headers python3-dev pcre-dev
RUN apk del py3-pip
RUN chmod -R a+rX /app

CMD ["uwsgi", "--ini", "uwsgi.ini"]
#CMD ["python3", "flask_uwsgi.py"]

